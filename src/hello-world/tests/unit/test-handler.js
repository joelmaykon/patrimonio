'use strict'

const chai = require('chai')
const expect = chai.expect
var event, context

describe('Tests index', async function () {
  it('verifies successful response', async () => {
    const result = {
      statusCode: 200,
      body: JSON.stringify({
        message: 'hello world',
      }),
    }

    expect(result).to.be.an('object')
    expect(result.statusCode).to.equal(200)
    expect(result.body).to.be.an('string')

    let response = JSON.parse(result.body)

    expect(response).to.be.an('object')
    expect(response.message).to.be.equal('hello world')
  })
})
